----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:06:51 12/05/2017 
-- Design Name: 
-- Module Name:    AND_GATE3TO1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ANDGATE3TO1 is
    Port ( a_and : in  STD_LOGIC;
           b_and : in  STD_LOGIC;
           c_and : in  STD_LOGIC;
           out_and : out  STD_LOGIC);
end ANDGATE3TO1;

architecture Behavioral of AND_GATE3TO1 is

begin


end Behavioral;

