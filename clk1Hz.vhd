----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:32:56 12/05/2017 
-- Design Name: 
-- Module Name:    clk1Hz - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk1Hz is
    Port ( pulso : in  STD_LOGIC;
           clk1hz : inout  STD_LOGIC);
end clk1Hz;

architecture Behavioral of clk1Hz is
signal count: INTEGER:=1;
constant max_count: INTEGER := 50000000;
begin
process(clk1hz, pulso, count)
	begin
		if pulso'event and pulso='1' then
			if count < max_count then 
				count <= count+1;
			else
				clk1hz <= not clk1hz;
				count <= 0;
			end if;
		end if;
	end process;

end Behavioral;

