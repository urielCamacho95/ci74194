----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:12:18 12/05/2017 
-- Design Name: 
-- Module Name:    FF_D - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FF_D is
    Port ( ffd_d : in  STD_LOGIC;
           ffd_clk : in  STD_LOGIC;
			  ffd_reset : in STD_LOGIC;
           ffd_q : out  STD_LOGIC;
			  ffd_qx : inout STD_LOGIC);
end FF_D;

architecture Behavioral of FF_D is
	signal sig1,sig2,sig3,sig4 : STD_LOGIC;
	component NAND_GATE
		Port ( nand_a : in  STD_LOGIC;
           nand_b : in  STD_LOGIC;
           nand_out : out  STD_LOGIC);
	end component;
	component NOT_GATE
		Port ( not_in : in  STD_LOGIC;
           not_out : out  STD_LOGIC);
	end component;
begin
	not_gate1 : NOT_GATE port map(ffd_d,sig1);
	nand_gate1 : NAND_GATE port map(ffd_d,ffd_clk,sig2);
	nand_gate2 : NAND_GATE port map(sig1,ffd_clk,sig3);
	nand_gate4 : NAND_GATE port map(sig3,sig4,ffd_qx);
	nand_gate3 : NAND_GATE port map(sig2,ffd_qx,sig4);
	process (ffd_clk,ffd_reset)
	begin
		if (ffd_reset = '1') then
			ffd_q<='0';
		else
			if rising_edge(ffd_clk) then
				ffd_q<=sig4;
			end if;
		end if;
	end process;
end Behavioral;

