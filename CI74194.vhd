----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:29:48 12/05/2017 
-- Design Name: 
-- Module Name:    CI74194 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CI74194 is
    Port ( S0 : in  STD_LOGIC;
           S1 : in  STD_LOGIC;
           DSR : in  STD_LOGIC;
           DSL : in  STD_LOGIC;
           P0 : in  STD_LOGIC;
           P1 : in  STD_LOGIC;
           P2 : in  STD_LOGIC;
           P3 : in  STD_LOGIC;
           P4 : in  STD_LOGIC;
           P5 : in  STD_LOGIC;
           P6 : in  STD_LOGIC;
           P7 : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           MR : in  STD_LOGIC;
           Q0 : inout  STD_LOGIC;
           Q1 : inout  STD_LOGIC;
           Q2 : inout  STD_LOGIC;
           Q3 : inout  STD_LOGIC;
           Q4 : inout  STD_LOGIC;
           Q5 : inout  STD_LOGIC;
           Q6 : inout  STD_LOGIC;
           Q7 : inout  STD_LOGIC);
end CI74194;

architecture Behavioral of CI74194 is
signal signal_clock,sig1,sig2,sig3,sig4,sig5,sig6,sig7,sig8,sig9,sig10 : STD_LOGIC;
signal sig11,sig12,sig13,sig14,sig15,sig16,sig17,sig18,sig19,sig20,sig21 : STD_LOGIC;
signal sig22,sig23,sig24,sig25,sig26,sig27,sig28,sig29,sig30,sig31,sig32 : STD_LOGIC;
signal sig33,sig34,sig35,sig36,sig37,sig38,sig39,sig40,sig41,sig42,sig43 : STD_LOGIC;
signal sig44,sig45,sig46,sig47,sig48,sig49,sig50,sig51,sig52 : STD_LOGIC;
	component clk1Hz
		Port ( pulso : in  STD_LOGIC;
           clk1hz : inout  STD_LOGIC);
	end component;
	component NOT_GATE
		Port ( not_in : in  STD_LOGIC;
           not_out : out  STD_LOGIC);
	end component;
	component NOR_GATE4TO1
		Port ( a_nor : in  STD_LOGIC;
           b_nor : in  STD_LOGIC;
           c_nor : in  STD_LOGIC;
           d_nor : in  STD_LOGIC;
           nor_out : inout  STD_LOGIC);
	end component;
	component AND3TO1
		Port ( ina : in  STD_LOGIC;
           inb : in  STD_LOGIC;
           inc : in  STD_LOGIC;
           outand : inout  STD_LOGIC);
	end component;
	component FF_D 
		Port ( ffd_d : in  STD_LOGIC;
			ffd_clk : in  STD_LOGIC;
			ffd_reset : in STD_LOGIC;
			ffd_q : out  STD_LOGIC;
			ffd_qx : inout STD_LOGIC);
	end component;
begin

	--signal_clock : senal con divisor de frecuencia
	--reloj1Hz : clk1Hz port map(CLK, signal_clock);
	not1 : NOT_GATE port map(S1,sig1);
	--sig1, salida negada de s1
	not2 : NOT_GATE port map(sig1,sig2);
	--sig2: s1 --
	not3 : NOT_GATE port map(S0,sig3);
	
	not4 : NOT_GATE port map(sig3,sig4);
	
	andgate1 : AND3TO1 port map(DSR,sig1,sig4,sig5);
	andgate2 : AND3TO1 port map(sig4,sig2,P0,sig6);
	andgate3 : AND3TO1 port map(sig3,sig2,Q1,sig7);
	andgate4 : AND3TO1 port map(sig4,sig1,Q0,sig8);
	
	andgate5 : AND3TO1 port map(Q0,sig1,sig4,sig9);
	andgate6 : AND3TO1 port map(sig3,sig2,P1,sig10);
	andgate7 : AND3TO1 port map(sig4,sig2,Q2,sig11);
	andgate8 : AND3TO1 port map(sig4,sig1,Q1,sig12);
	
	andgate9 : AND3TO1 port map(Q1,sig1,sig4,sig13);
	andgate10 : AND3TO1 port map(sig3,sig2,P2,sig14);
	andgate11 : AND3TO1 port map(sig4,sig2,Q3,sig15);
	andgate12 : AND3TO1 port map(sig4,sig1,Q2,sig16);
	
	andgate13 : AND3TO1 port map(Q2,sig1,sig4,sig17);
	andgate14 : AND3TO1 port map(sig3,sig2,P3,sig18);
	andgate15 : AND3TO1 port map(sig4,sig2,Q4,sig19);
	andgate16 : AND3TO1 port map(sig4,sig1,Q3,sig20);
	
	andgate17 : AND3TO1 port map(Q3,sig1,sig4,sig21);
	andgate18 : AND3TO1 port map(sig3,sig2,P4,sig22);
	andgate19 : AND3TO1 port map(sig4,sig2,Q5,sig23);
	andgate20 : AND3TO1 port map(sig4,sig1,Q4,sig24);
	
	andgate21 : AND3TO1 port map(Q4,sig1,sig4,sig25);
	andgate22 : AND3TO1 port map(sig3,sig2,P5,sig26);
	andgate23 : AND3TO1 port map(sig4,sig2,Q6,sig27);
	andgate24 : AND3TO1 port map(sig4,sig1,Q5,sig28);
	
	andgate25 : AND3TO1 port map(Q5,sig1,sig4,sig29);
	andgate26 : AND3TO1 port map(sig3,sig2,P6,sig30);
	andgate27 : AND3TO1 port map(sig4,sig2,Q7,sig31);
	andgate28 : AND3TO1 port map(sig4,sig1,Q6,sig32);
	
	andgate29 : AND3TO1 port map(Q6,sig1,sig4,sig33);
	andgate30 : AND3TO1 port map(sig4,sig2,P7,sig34);
	andgate31 : AND3TO1 port map(sig3,sig2,DSL,sig35);
	andgate32 : AND3TO1 port map(sig3,sig1,Q7,sig36);
	
	--
	--nor gates
	norgate1 : NOR_GATE4TO1 port map(sig5,sig6,sig7,sig8,sig37);
	norgate2 : NOR_GATE4TO1 port map(sig9,sig10,sig11,sig12,sig38);
	norgate3 : NOR_GATE4TO1 port map(sig13,sig14,sig15,sig16,sig39);
	norgate4 : NOR_GATE4TO1 port map(sig17,sig18,sig19,sig20,sig40);
	norgate5 : NOR_GATE4TO1 port map(sig21,sig22,sig23,sig24,sig41);
	norgate6 : NOR_GATE4TO1 port map(sig25,sig26,sig27,sig28,sig42);
	norgate7 : NOR_GATE4TO1 port map(sig29,sig30,sig31,sig32,sig43);
	norgate8 : NOR_GATE4TO1 port map(sig33,sig34,sig35,sig36,sig44);
	
	--ffd (8)
	ff0 : FF_D port map(sig37,CLK,MR,Q0,sig45);
	ff1 : FF_D port map(sig38,CLK,MR,Q1,sig46);
	ff2 : FF_D port map(sig39,CLK,MR,Q2,sig47);
	ff3 : FF_D port map(sig40,CLK,MR,Q3,sig48);
	ff4 : FF_D port map(sig41,CLK,MR,Q4, sig49);
	ff5 : FF_D port map(sig42,CLK,MR,Q5, sig50);
	ff6 : FF_D port map(sig43,CLK,MR,Q6, sig51);
	ff7 : FF_D port map(sig44,CLK,MR,Q7, sig52);
	
end Behavioral;