----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:27:58 12/05/2017 
-- Design Name: 
-- Module Name:    NOR_GATE4TO1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NOR_GATE4TO1 is
    Port ( a_nor : in  STD_LOGIC;
           b_nor : in  STD_LOGIC;
           c_nor : in  STD_LOGIC;
           d_nor : in  STD_LOGIC;
           nor_out : inout  STD_LOGIC);
end NOR_GATE4TO1;

architecture Behavioral of NOR_GATE4TO1 is

begin
	nor_out <= a_nor or (b_nor or (c_nor or d_nor));
end Behavioral;

