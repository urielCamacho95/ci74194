--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:59:18 12/05/2017
-- Design Name:   
-- Module Name:   /home/uriel/CI74194/test74194.vhd
-- Project Name:  CI74194
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: CI74194
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test74194 IS
END test74194;
 
ARCHITECTURE behavior OF test74194 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT CI74194
    PORT(
         S0 : IN  std_logic;
         S1 : IN  std_logic;
         DSR : IN  std_logic;
         DSL : IN  std_logic;
         P0 : IN  std_logic;
         P1 : IN  std_logic;
         P2 : IN  std_logic;
         P3 : IN  std_logic;
         P4 : IN  std_logic;
         P5 : IN  std_logic;
         P6 : IN  std_logic;
         P7 : IN  std_logic;
         CLK : IN  std_logic;
         MR : IN  std_logic;
         Q0 : INOUT  std_logic;
         Q1 : INOUT  std_logic;
         Q2 : INOUT  std_logic;
         Q3 : INOUT  std_logic;
         Q4 : INOUT  std_logic;
         Q5 : INOUT  std_logic;
         Q6 : INOUT  std_logic;
         Q7 : INOUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal S0 : std_logic := '0';
   signal S1 : std_logic := '1';
   signal DSR : std_logic := '0';
   signal DSL : std_logic := '0';
   signal P0 : std_logic := '1';
   signal P1 : std_logic := '0';
   signal P2 : std_logic := '0';
   signal P3 : std_logic := '0';
   signal P4 : std_logic := '0';
   signal P5 : std_logic := '0';
   signal P6 : std_logic := '0';
   signal P7 : std_logic := '0';
   signal CLK : std_logic := '0';
   signal MR : std_logic := '0';

	--BiDirs
   signal Q0 : std_logic;
   signal Q1 : std_logic;
   signal Q2 : std_logic;
   signal Q3 : std_logic;
   signal Q4 : std_logic;
   signal Q5 : std_logic;
   signal Q6 : std_logic;
   signal Q7 : std_logic;

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: CI74194 PORT MAP (
          S0 => S0,
          S1 => S1,
          DSR => DSR,
          DSL => DSL,
          P0 => P0,
          P1 => P1,
          P2 => P2,
          P3 => P3,
          P4 => P4,
          P5 => P5,
          P6 => P6,
          P7 => P7,
          CLK => CLK,
          MR => MR,
          Q0 => Q0,
          Q1 => Q1,
          Q2 => Q2,
          Q3 => Q3,
          Q4 => Q4,
          Q5 => Q5,
          Q6 => Q6,
          Q7 => Q7
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
