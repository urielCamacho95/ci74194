--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:38:48 12/05/2017
-- Design Name:   
-- Module Name:   /home/uriel/CI74194/test_ffd.vhd
-- Project Name:  CI74194
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: FF_D
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_ffd IS
END test_ffd;
 
ARCHITECTURE behavior OF test_ffd IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT FF_D
    PORT(
         ffd_d : IN  std_logic;
         ffd_clk : INOUT  std_logic;
         ffd_reset : IN  std_logic;
         ffd_q : INOUT  std_logic;
         ffd_qx : INOUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal ffd_d : std_logic := '1';
   signal ffd_reset : std_logic := '0';

	--BiDirs
   signal ffd_clk : std_logic;
   signal ffd_q : std_logic;
   signal ffd_qx : std_logic;

   -- Clock period definitions
   constant ffd_clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: FF_D PORT MAP (
          ffd_d => ffd_d,
          ffd_clk => ffd_clk,
          ffd_reset => ffd_reset,
          ffd_q => ffd_q,
          ffd_qx => ffd_qx
        );

   -- Clock process definitions
   ffd_clk_process :process
   begin
		ffd_clk <= '0';
		wait for ffd_clk_period/2;
		ffd_clk <= '1';
		wait for ffd_clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for ffd_clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
